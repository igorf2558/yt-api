import os



basedir = os.path.abspath(os.path.dirname(__file__))
UTAPI = 'AIzaSyADAa-zaknNiYk1n2WPwWZ2JMRRe49XEgc'


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'supersecretkey321'

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
