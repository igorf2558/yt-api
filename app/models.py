from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login


videosearching = db.Table('videosearching',
    db.Column('videosearch_id', db.Integer, db.ForeignKey('videosearch.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'))
)


class User(UserMixin, db.Model):

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    # videosearch = db.relationship('VideoSearch', backref='videosearch_id',
    #                                lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @login.user_loader
    def load_user(id):
        return User.query.get(int(id))



class VideoSearch(db.Model):

    __tablename__ = 'videosearch'

    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(50))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    children = db.relationship("User",
                    secondary=videosearching)
