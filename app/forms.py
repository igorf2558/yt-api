from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, Form, SelectField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User, VideoSearch, videosearching
# from config import UTAPI
import requests
import json
from app import db
from flask_login import current_user

UTAPI = 'AIzaSyDzXaPfP-RVqD1wCB3x5SsmN9yAOS0Q6U4'


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')



class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')



class SearchVideoForm(FlaskForm):
    search = StringField('search')
    submit = SubmitField('GO!')

    def search_video(search):
        payload = {'part': 'snippet', 'key': UTAPI, 'order':'viewCount',
                   'q': search, 'maxResults': 4}
        l = requests.Session().get('https://www.googleapis.com/youtube/v3/search',
                                    params=payload)
        resp_dict = json.loads(l.content)
        # print(resp_dict)
        for item in resp_dict['items']:
            if item['id']['kind'] != 'youtube#video':
                continue
            token = resp_dict['items'][0]['id']['videoId']


            s = User()
            print(s)
            c = VideoSearch()
            c.children.append(s)
            db.session.add(c)
            db.session.commit()
            # token = videosearching(videosearch_id = videosearch.id, user_id = current_user.id)
            # print(token)
            # db.session.add(token)
            # db.session.commit()



class LikeButton(FlaskForm):
    submit = SubmitField('LIKE')

    def likevideo():
        pass
