
# -*- coding: utf-8 -*-

from app import app, models
from flask import render_template, flash, redirect, url_for
from app.forms import LoginForm, RegistrationForm, SearchVideoForm, LikeButton
from flask_login import current_user, login_user
from app.models import User, VideoSearch
from flask_login import logout_user
from app import db
from flask import request
from werkzeug.urls import url_parse
import requests
import json



@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    form = SearchVideoForm()
    if form.is_submitted():
        search = form.search.data
        SearchVideoForm.search_video(search)
        # u = current_user
        # users = u.videosearch.all()
        # for user in users:
        #     print(user)
        return redirect(url_for('index'))
    return render_template('index.html', title = 'index', form = form)


@app.route('/respone', methods=['GET', 'POST'])
def respone():
    form = LikeButton()
    # if form.is_submitted():
    #     video_id = models.VideoLikes(video_token = token, id_user = User.id)
    #     print(video_id)
    #     db.session.add(video_id)
    #     db.session.commit()
    #     return redirect(url_for('likes'))
    return render_template('respone.html', title = 'respone', form = form,
                            items = current_user.videosearch.token.all())


@app.route('/likes', methods=['GET', 'POST'])
def likes():
    # likes = User.video_id.all()
    # for like in likes:
    #     print (like)
    return render_template('likes.html', title='likes', likes = likes)



@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)



@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))



@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)
